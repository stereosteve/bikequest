package cb

import (
	"encoding/json"
	"fmt"
	"time"
)

type AnonRide struct {
	Duration             int64    `json:"tripduration"`
	StartTime            JSONTime `json:"starttime"`
	StopTime             JSONTime `json:"stoptime"`
	StartStationID       string   `json:"start_station_id"`
	StartStationName     string   `json:"start_station_name"`
	StartStationLocation Location `json:"start_station_location"`
	EndStationID         string   `json:"end_station_id"`
	EndStationName       string   `json:"end_station_name"`
	EndStationLocation   Location `json:"end_station_location"`
	BikeID               string   `json:"bikeid"`
	UserType             string   `json:"usertype"`
	BirthYear            int      `json:"birth_year"`
	Gender               int      `json:"gender"`
	UserName             string   `json:"username"`
	RouteID              string   `json:"route_id"`
	RouteRank            int      `json:"route_rank"`
}

func (a AnonRide) String() string {
	return fmt.Sprintf("%s -> %s on %s duration %d", a.StartStationName, a.EndStationName, a.StartTime, a.Duration)
}

func (a AnonRide) GenderString() string {
	switch a.Gender {
	case 1:
		return "M"
	case 2:
		return "F"
	default:
		return ""
	}
}

func (a AnonRide) DurationString() string {
	d := time.Duration(a.Duration) * time.Second
	return d.String()
}

type Location struct {
	Lat json.Number `json:"lat"`
	Lon json.Number `json:"lon"`
}

func (l Location) String() string {
	return fmt.Sprintf("%s,%s", l.Lat.String(), l.Lon.String())
}
