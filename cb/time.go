package cb

import (
	"fmt"
	"time"
)

type JSONTime time.Time

func (t JSONTime) MarshalJSON() ([]byte, error) {
	stamp := time.Time(t).Format(`"2006-01-02 15:04:05"`)
	return []byte(stamp), nil
}

func (t *JSONTime) UnmarshalJSON(b []byte) error {
	s := string(b)
	if len(s) > 2 {
		// remove leading and trailing " from JSON string
		*t = ParseDate(s[1 : len(s)-1])
	}
	return nil
}

func (t JSONTime) String() string {
	return time.Time(t).Format("2006-01-02 15:04:05")
}

func (t JSONTime) PrettyDate() string {
	return time.Time(t).Format("Mon, Jan 02, 2006")
}
func (t JSONTime) PrettyTime() string {
	return time.Time(t).Format(time.Kitchen)
}

// ParseDate can handle the variety of citibike date formats
// taken from spf13/cast
func ParseDate(s string) JSONTime {
	t, err := parseDateWith(s, []string{
		"01/02/2006 3:04:05 PM",
		"01/02/2006 03:04:05 PM",
		"2006-01-02 15:04:05",
		"01/02/2006 15:04:05",
		"01/02/2006 15:04",
		"1/2/2006 15:04:05",
		"1/2/2006 15:04",
	})
	if err != nil {
		panic(err)
	}
	return JSONTime(t)
}

func parseDateWith(s string, dates []string) (d time.Time, e error) {
	for _, dateType := range dates {
		if d, e = time.Parse(dateType, s); e == nil {
			return
		}
	}
	return d, fmt.Errorf("Unable to parse date: %s", s)
}
