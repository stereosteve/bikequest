package cb

import "testing"

func TestJSONTime(t *testing.T) {
	d := ParseDate("2013-07-01 07:42:18")

	b, err := d.MarshalJSON()
	if err != nil {
		t.Error(err)
	}

	if string(b) != `"2013-07-01 07:42:18"` {
		t.Fatalf("expected %s got %s", `"2013-07-01 07:42:18"`, b)
	}
}
