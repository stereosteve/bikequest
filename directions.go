package main

import (
	"bikequest/es"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
)

// example of looking up bike directions in google directions API
// does the first 10 trips as a simple example
func directions() {
	os.Mkdir("directions", 0777)

	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Size(10).
		Do(context.TODO())

	if err != nil {
		panic(err)
	}

	rides := pluckRides(sr)

	seenRoute := make(map[string]bool)

	for _, ride := range rides {
		routeID := ride.StartStationID + "_" + ride.EndStationID
		if seenRoute[routeID] {
			continue
		}
		seenRoute[routeID] = true

		vals := &url.Values{}
		vals.Set("origin", ride.StartStationLocation.String())
		vals.Set("destination", ride.EndStationLocation.String())
		vals.Set("mode", "bicycling")
		vals.Set("key", "AIzaSyBAsdGLGFJfUQ2mU4jLW2FYxG5x96wuA3E")
		u := "https://maps.googleapis.com/maps/api/directions/json?" + vals.Encode()

		fmt.Println("---")
		fmt.Println(u)

		resp, err := http.Get(u)
		if err != nil {
			panic(err)
		}

		f, err := os.Create("directions/" + routeID + ".json")
		if err != nil {
			panic(err)
		}

		_, err = io.Copy(f, resp.Body)
		if err != nil {
			panic(err)
		}

		resp.Body.Close()
		f.Close()

	}
}
