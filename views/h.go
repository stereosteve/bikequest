package views

import (
	"time"

	"github.com/dustin/go-humanize"
)

var (
	comma = humanize.Comma
)

type Session struct {
	UserName string
}

func commaNotZero(num int64) string {
	if num != 0 {
		return comma(num)
	}
	return ""
}

func dateFromTS(ts float64) string {
	t := time.Unix(int64(ts)/1000, 0)
	return t.UTC().Format("2006-01-02")
}
