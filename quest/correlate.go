package quest

import (
	"bikequest/cb"
	"bikequest/es"
	"bikequest/station"
	"context"
	"encoding/json"
	"fmt"

	"gopkg.in/olivere/elastic.v5"
)

func correlate(user *User, quest *Quest) {

	quest.UserName = user.UserName

	quest.Origin, _ = station.Find(quest.StartStation)
	quest.Destination, _ = station.Find(quest.EndStation)
	if quest.Origin != nil && quest.Destination != nil {
		quest.RouteID = fmt.Sprintf("%s_%s", quest.Origin.ID, quest.Destination.ID)
	}

	dsl := elastic.NewBoolQuery().
		Must(elastic.NewTermQuery("birth_year", user.BirthYear())).
		Must(elastic.NewTermQuery("gender", user.GenderInt())).
		Must(elastic.NewRangeQuery("starttime").From(quest.Date.String()+"||-2s").To(quest.Date.String()+"||+2s")).
		Must(elastic.NewRangeQuery("tripduration").From(quest.Duration-2).To(quest.Duration+2)).
		Should(
			elastic.NewTermQuery("start_station_name", quest.StartStation),
			elastic.NewTermQuery("end_station_name", quest.EndStation),
		)

	sr, err := es.Client.Search(es.TripIndex).Type(es.TripType).Query(dsl).Do(context.TODO())
	if err != nil {
		panic(err)
	}

	if sr.TotalHits() > 0 {
		h := sr.Hits.Hits[0]
		anonRide := &cb.AnonRide{}
		err := json.Unmarshal(*h.Source, &anonRide)
		if err != nil {
			panic(err)
		}

		quest.TripID = h.Id

		// populate username field
		anonRide.UserName = user.UserName
		update := elastic.NewBulkUpdateRequest().
			Index(es.TripIndex).
			Type(es.TripType).
			Id(h.Id).
			Doc(anonRide)
		es.Processor.Add(update)
	}
}

func PprintQuery(ss elastic.Query) {
	source, _ := ss.Source()
	data, _ := json.MarshalIndent(source, "", "  ")
	fmt.Println(string(data))
}
