package quest

import (
	"bikequest/es"
	"fmt"
	"os"
	"testing"
)

func TestLogin(t *testing.T) {
	t.Skip()

	sj := NewScraper()
	user, err := sj.Login(os.Getenv("CITIBIKE_USERNAME"), os.Getenv("CITIBIKE_PASSWORD"))
	if err != nil {
		panic(err)
	}
	fmt.Println(user)

	tp, err := sj.tripsPage(user.QuestsURL)
	fmt.Println(tp)

}

func TestImportAllTrips(t *testing.T) {
	t.Skip()

	es.Dial()

	sj := NewScraper()
	user, err := sj.Login(os.Getenv("CITIBIKE_USERNAME"), os.Getenv("CITIBIKE_PASSWORD"))
	if err != nil {
		panic(err)
	}
	fmt.Println(user)

	tripPagesChan := make(chan *QuestsPage)
	go sj.ImportAllTrips(tripPagesChan)
	for page := range tripPagesChan {
		fmt.Println(page)
	}

}
