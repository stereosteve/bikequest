package quest

import (
	"bikequest/es"
	"time"

	"gopkg.in/olivere/elastic.v5"
)

type User struct {
	FirstName   string
	LastName    string
	UserName    string
	Email       string
	Gender      string
	MemberSince string
	DOB         string
	QuestsURL   string
}

func (u *User) BirthYear() int {
	d, err := time.Parse("January 2, 2006", u.DOB)
	if err != nil {
		panic(err)
	}
	return d.Year()
}

func (u *User) GenderInt() int {
	if u.Gender == "Male" {
		return 1
	} else if u.Gender == "Female" {
		return 2
	}
	return 0
}

func (u *User) Save() {
	update := elastic.NewBulkUpdateRequest().
		Index("priv").
		Type("user").
		Id(u.UserName).
		Doc(u).DocAsUpsert(true)
	es.Processor.Add(update)
}
