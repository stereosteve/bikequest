package quest

import (
	"bikequest/cb"
	"bikequest/es"
	"bikequest/station"
	"fmt"
	"time"

	"gopkg.in/olivere/elastic.v5"
)

type Quest struct {
	UserName     string           `json:"username"`
	StartStation string           `json:"startStation"`
	EndStation   string           `json:"endStation"`
	Date         cb.JSONTime      `json:"date"`
	Duration     int              `json:"durationSeconds"`
	TripID       string           `json:"trip_id"`
	RouteID      string           `json:"route_id"`
	Origin       *station.Station `json:"origin"`
	Destination  *station.Station `json:"destination"`
}

type QuestsPage struct {
	CurPage     string
	NumPages    string
	nextPageURL string
	Quests      []Quest
}

func (t Quest) Save() {
	id := fmt.Sprintf("%s_%s_%s_%s", t.UserName, t.Date.String(), t.StartStation, t.EndStation)
	update := elastic.NewBulkUpdateRequest().
		Index("priv").
		Type("quest").
		Id(id).
		Doc(t).DocAsUpsert(true)
	es.Processor.Add(update)
}

func (q Quest) DurationString() string {
	d := time.Duration(q.Duration) * time.Second
	return d.String()
}
