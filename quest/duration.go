package quest

import (
	"regexp"
	"strconv"
)

var (
	reSec  = regexp.MustCompile("([0-9]+) s")
	reMin  = regexp.MustCompile("([0-9]+) min")
	reHour = regexp.MustCompile("([0-9]+) h")
)

func parseDuration(in string) int {
	seconds := 0

	if m := reSec.FindStringSubmatch(in); len(m) == 2 {
		val, _ := strconv.Atoi(m[1])
		seconds += val
	}

	if m := reMin.FindStringSubmatch(in); len(m) == 2 {
		val, _ := strconv.Atoi(m[1])
		seconds += (val * 60)
	}

	if m := reHour.FindStringSubmatch(in); len(m) == 2 {
		val, _ := strconv.Atoi(m[1])
		seconds += (val * 3600)
	}

	return seconds
}
