package quest

import (
	"bikequest/cb"
	"bikequest/es"
	"errors"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"time"

	"fmt"

	"github.com/PuerkitoBio/goquery"
)

type Scraper struct {
	client *http.Client
	User   *User
}

func NewScraper() *Scraper {
	sj := &Scraper{}

	cookieJar, _ := cookiejar.New(nil)

	sj.client = &http.Client{
		Jar: cookieJar,
	}

	return sj
}

// Login submits the login form and sets up the client
func (sj *Scraper) Login(username, password string) (*User, error) {

	resp, err := sj.client.Get("https://member.citibikenyc.com/profile/login")
	if err != nil {
		fmt.Println("1", err)
		return nil, err
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		fmt.Println("2", err)
		return nil, err
	}

	tok, ok := doc.Find(`[name="_login_csrf_security_token"]`).First().Attr("value")
	if !ok {
		panic("failed to get token")
	}

	// POST
	values := make(url.Values)
	values.Set("_username", username)
	values.Set("_password", password)
	values.Set("_login_csrf_security_token", tok)

	r2, err := sj.client.PostForm("https://member.citibikenyc.com/profile/login_check", values)
	if err != nil {
		fmt.Println("3", err)
		return nil, err
	}

	doc, err = goquery.NewDocumentFromResponse(r2)
	if err != nil {
		fmt.Println("4", err)
		return nil, err
	}

	u, err := sj.parseUser(doc)
	if err != nil {
		fmt.Println("5", err)
		return nil, err
	}

	sj.User = u
	return u, nil
}

// ImportAllTrips scrapes all trip pages.
// Login must be called before ImportAllTrips
// to setup the client.
func (sj *Scraper) ImportAllTrips(pages chan *QuestsPage) {
	sj.User.Save()

	n := 0

	for tripURL := sj.User.QuestsURL; tripURL != ""; {
		tp, err := sj.tripsPage(tripURL)
		if err != nil {
			panic(err)
		}

		for _, t := range tp.Quests {
			t.UserName = sj.User.UserName
			correlate(sj.User, &t)
			t.Save()
		}

		pages <- tp
		tripURL = tp.nextPageURL

		n++
		// if n > 5 {
		// 	break
		// }
	}

	err := es.Processor.Flush()
	if err != nil {
		panic(err)
	}

	close(pages)
}

// parseUser extracts user info from landing page
func (sj *Scraper) parseUser(doc *goquery.Document) (*User, error) {
	u := &User{}
	u.FirstName = docGetText(doc.Selection, ".ed-panel__info__value_firstname")
	u.LastName = docGetText(doc.Selection, ".ed-panel__info__value_lastname")
	u.UserName = docGetText(doc.Selection, ".ed-panel__info__value_username")
	u.Email = docGetText(doc.Selection, ".ed-panel__info__value_email")
	u.DOB = docGetText(doc.Selection, ".ed-panel__info__value_date-of-birth")
	u.Gender = docGetText(doc.Selection, ".ed-panel__info__value_gender")
	u.MemberSince = docGetText(doc.Selection, ".ed-panel__info__value_member-since")
	href, _ := doc.Find(".ed-profile-menu__link_trips a").First().Attr("href")
	u.QuestsURL = "https://member.citibikenyc.com" + href

	if u.Email == "" {
		return nil, errors.New("Citi-Bike Login failed. Check username and password.")
	}
	if u.UserName == "" {
		u.UserName = fmt.Sprintf("%s%s", u.FirstName, u.LastName)
	}
	return u, nil
}

// tripsPage requests a trip page URL and parses trips
func (sj *Scraper) tripsPage(url string) (*QuestsPage, error) {

	resp, err := sj.client.Get(url)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return nil, err
	}

	return sj.parseTripsPage(doc)
}

// parseTripsPage extracts list of trips
func (sj *Scraper) parseTripsPage(doc *goquery.Document) (*QuestsPage, error) {
	tp := &QuestsPage{}

	doc.Find(".ed-table__item").Each(func(i int, s *goquery.Selection) {
		trip := Quest{}
		trip.StartStation = docGetText(s, ".ed-table__item__info__sub-info_trip-start-station")
		trip.EndStation = docGetText(s, ".ed-table__item__info__sub-info_trip-end-station")

		parsedDate, err := time.Parse("01/02/2006 3:04:05 PM", docGetText(s, ".ed-table__item__info__sub-info_trip-start-date"))
		if err != nil {
			panic(err)
		}
		trip.Date = cb.JSONTime(parsedDate)

		dur := docGetText(s, ".ed-table__item__info_trip-duration")
		trip.Duration = parseDuration(dur)
		tp.Quests = append(tp.Quests, trip)
	})

	currPage, _ := doc.Find(".ed-paginated-navigation__jump-to__page").First().Attr("value")
	numPages := docGetText(doc.Selection, ".ed-paginated-navigation__jump-to__last-page")
	tp.CurPage = currPage
	tp.NumPages = numPages

	if currPage != numPages {
		href, _ := doc.Find(".ed-paginated-navigation__pages-group__link_next").First().Attr("href")
		tp.nextPageURL = "https://member.citibikenyc.com" + href
	}

	return tp, nil
}

// ------------------------
// misc
//
func docGetText(doc *goquery.Selection, sel string) string {
	val := doc.Find(sel).First().Text()
	return strings.TrimSpace(val)
}

func docGetInt(doc *goquery.Selection, sel string) int {
	sv := docGetText(doc, sel)
	val, _ := strconv.Atoi(sv)
	return val
}
