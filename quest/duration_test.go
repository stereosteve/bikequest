package quest

import "testing"

func TestParseDuration(t *testing.T) {
	cases := []struct {
		in       string
		expected int
	}{
		{"33 s", 33},
		{"20 min 49 s", 1249},
		{"2 h 10 min 3 s", 7803},
		{"bogus", 0},
	}

	for _, c := range cases {
		actual := parseDuration(c.in)
		if actual != c.expected {
			t.Errorf("On %s Expected %d Got %d", c.in, c.expected, actual)
		}
	}
}
