package quest

import (
	"bikequest/es"
	"bikequest/station"
	"fmt"
	"os"
	"testing"

	"github.com/PuerkitoBio/goquery"
)

func TestParseUser(t *testing.T) {
	f, err := os.Open("testdata/landing.html")
	if err != nil {
		t.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromReader(f)

	sj := NewScraper()
	user, err := sj.parseUser(doc)

	fmt.Println(user, err)
}

func TestParseTrips(t *testing.T) {
	f, err := os.Open("testdata/trips.html")
	if err != nil {
		t.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromReader(f)

	sj := NewScraper()
	tp, err := sj.parseTripsPage(doc)

	fmt.Println(tp, err)
}

func TestSaveTrips(t *testing.T) {
	es.Dial()
	station.Load()

	tp := loadTestTrips()
	user := loadTestUser()
	fmt.Println(user)
	user.Save()

	for _, t := range tp.Quests {
		fmt.Println(t)
		correlate(user, &t)
		t.Save()
	}

	es.Processor.Flush()

}

// -------------------------------------------

func loadTestTrips() *QuestsPage {
	f, err := os.Open("testdata/tripsLastPage.html")
	if err != nil {
		panic(err)
	}
	sj := NewScraper()
	doc, err := goquery.NewDocumentFromReader(f)
	tp, err := sj.parseTripsPage(doc)
	if err != nil {
		panic(err)
	}
	return tp
}

func loadTestUser() *User {
	f, err := os.Open("testdata/landing.html")
	if err != nil {
		panic(err)
	}
	sj := NewScraper()
	doc, err := goquery.NewDocumentFromReader(f)
	user, err := sj.parseUser(doc)
	if err != nil {
		panic(err)
	}
	return user
}
