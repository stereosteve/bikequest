package main

import (
	"bikequest/cb"
	"bikequest/es"
	"bikequest/quest"
	"bikequest/route"
	"bikequest/station"
	"bikequest/views"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"reflect"

	"github.com/alexedwards/scs/engine/boltstore"
	"github.com/alexedwards/scs/session"
	"github.com/boltdb/bolt"
	"github.com/stereosteve/letsencrypt"

	"gopkg.in/olivere/elastic.v5"
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	sess := getSess(r)
	views.WriteHome(w, sess)
}

func serveRoutes(w http.ResponseWriter, r *http.Request) {
	sr, err := es.Client.Search("routes").
		Type("route").
		Size(1000).
		Sort("count", false).
		Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}

	var typ route.Route
	var routes []route.Route
	for _, item := range sr.Each(reflect.TypeOf(typ)) {
		if t, ok := item.(route.Route); ok {
			routes = append(routes, t)
		}
	}

	views.WriteLayout(w, &views.RoutesPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		SR:     sr,
		Routes: routes,
	})
}

func serveBetween(w http.ResponseWriter, r *http.Request) {
	start := r.URL.Query().Get("start")
	stop := r.URL.Query().Get("stop")
	if start == "" || stop == "" {
		http.Error(w, "start and stop are required", 400)
		return
	}

	dsl := elastic.NewBoolQuery().
		Must(elastic.NewTermQuery("start_station_name", start)).
		Must(elastic.NewTermQuery("end_station_name", stop))

	sr, err := es.Client.Search(es.TripIndex).Type(es.TripType).Query(dsl).Sort("tripduration", true).Size(200).Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}

	anonRides := pluckRides(sr)

	views.WriteLayout(w, &views.BetweenPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		Start: start,
		Stop:  stop,
		Rides: anonRides,
		SR:    sr,
	})
}

func serveUsers(w http.ResponseWriter, r *http.Request) {

	sr, err := es.Client.Search("trips").
		Type("trip").
		Size(0).
		Aggregation("users", elastic.NewTermsAggregation().Field("username").Size(-1)).
		Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}

	views.WriteLayout(w, &views.UsersPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		SR: sr,
	})
}

func serveUser(w http.ResponseWriter, r *http.Request) {

	username := r.URL.Query().Get("u")
	if username == "" {
		http.Error(w, "username required", 400)
		return
	}

	dsl := elastic.NewBoolQuery().Must(elastic.NewTermQuery("username", username))

	// // most rides in 24 hours
	tripsPerDay := elastic.NewDateHistogramAggregation().Field("date").Interval("day")
	tripsPerWeek := elastic.NewDateHistogramAggregation().Field("date").Interval("week")
	dayOfWeek := elastic.NewTermsAggregation().Script(elastic.NewScript("doc['date'].date.dayOfWeek").Lang("expression"))
	hourOfDay := elastic.NewTermsAggregation().Script(elastic.NewScript("doc['date'].date.hourOfDay").Lang("expression")).Size(24)

	sr, err := es.Client.Search("priv").
		Type("quest").
		Query(dsl).
		Aggregation("tripsPerDay", tripsPerDay).
		Aggregation("tripsPerWeek", tripsPerWeek).
		Aggregation("dayOfWeek", dayOfWeek).
		Aggregation("hourOfDay", hourOfDay).
		Sort("date", false).
		Size(1000).
		Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}

	// anonRides := pluckRides(sr)
	quests := pluckQuests(sr)

	// Day of Week chart
	dow := make([]int64, 7)
	if agg, ok := sr.Aggregations.Terms("dayOfWeek"); ok {
		for _, bucket := range agg.Buckets {
			if day, err := bucket.KeyNumber.Float64(); err == nil {
				dow[int(day)-1] = bucket.DocCount
			}
		}
	}

	// Hour of Day chart
	hod := make([]int64, 25)
	if agg, ok := sr.Aggregations.Terms("hourOfDay"); ok {
		for _, bucket := range agg.Buckets {
			if day, err := bucket.KeyNumber.Float64(); err == nil {
				hod[int(day)] = bucket.DocCount
			}
		}
	}

	// streak
	streak := 0
	var streakStart float64
	var dayMax int64
	var topDay float64
	if agg, ok := sr.Aggregations.Terms("tripsPerDay"); ok {
		curr := 0
		st := 0.0
		for _, bucket := range agg.Buckets {
			if bucket.DocCount > 0 {
				if curr == 0 {
					st = bucket.Key.(float64)
				}
				curr += 1
			} else {
				curr = 0
			}
			if curr > streak {
				streak = curr
				streakStart = st
			}
			// day max
			if bucket.DocCount > dayMax {
				dayMax = bucket.DocCount
				topDay = bucket.Key.(float64)
			}
		}
	}

	views.WriteLayout(w, &views.UserPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		UserName:    username,
		SR:          sr,
		Rides:       quests,
		DayOfWeek:   dow,
		HourOfDay:   hod,
		Streak:      streak,
		StreakStart: streakStart,
		DayMax:      dayMax,
		TopDay:      topDay,
	})
}

func serveStations(w http.ResponseWriter, r *http.Request) {
	sr, err := es.Client.Search("stations").
		Type("station").
		Size(1000).
		Sort("name", true).
		Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}

	stations := station.Pluck(sr)

	views.WriteLayout(w, &views.StationsPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		SR:       sr,
		Stations: stations,
	})
}

func serveStation(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")

	sta, ok := station.Find(name)
	if !ok {
		http.Error(w, "not found", 404)
		return
	}

	dsl := elastic.NewBoolQuery().Must(elastic.NewTermQuery("start_station_name", name))
	aggDsl := elastic.NewTermsAggregation().
		Field("end_station_name").
		Size(50)
	birthYearAgg := elastic.NewTermsAggregation().
		Field("birth_year").
		ExcludeTerms("0").
		Size(50)
	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Query(dsl).
		Aggregation("popularDestinations", aggDsl).
		Aggregation("birth_year", birthYearAgg).
		Sort("tripduration", true).
		Size(100).
		Do(context.TODO())

	if err != nil {
		serveErr(w, r, err)
		return
	}

	views.WriteLayout(w, &views.StationPage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		Name:    name,
		Station: sta,
		SR:      sr,
	})
}

func serveBike(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	dsl := elastic.NewBoolQuery().Must(elastic.NewTermQuery("bikeid", id))
	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Query(dsl).
		Aggregation("popularOrigins", betweenAgg(20, 10)).
		Sort("tripduration", true).
		Size(100).
		Do(context.TODO())
	if err != nil {
		serveErr(w, r, err)
		return
	}
	anonRides := pluckRides(sr)
	views.WriteLayout(w, &views.BikePage{
		BasePage: views.BasePage{
			Session: getSess(r),
		},
		ID:    id,
		Rides: anonRides,
		SR:    sr,
	})
}

func serveLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		username := r.FormValue("username")
		password := r.FormValue("password")

		sj := quest.NewScraper()
		user, err := sj.Login(username, password)

		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		user.Save()

		sess := &views.Session{
			UserName: user.UserName,
		}
		if err := session.PutObject(r, "sess", sess); err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		// explicitly save session since import uses Flush
		if err := session.Save(w, r); err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		if r.FormValue("import") == "true" {
			serveImport(sj, w, r)
		} else {
			http.Redirect(w, r, "/user?u="+user.UserName, 302)
		}

	} else {
		sess := getSess(r)
		views.WriteLogin(w, sess)
	}
}

func serveImport(sj *quest.Scraper, w http.ResponseWriter, r *http.Request) {
	views.WriteImport(w, sj.User.UserName)

	tripPagesChan := make(chan *quest.QuestsPage)
	go sj.ImportAllTrips(tripPagesChan)
	for page := range tripPagesChan {
		fmt.Fprintf(w, "<script>updateProgress(%s, %s);</script>", page.CurPage, page.NumPages)
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
	}

	fmt.Fprintf(w, "<script>done();</script>")

}

func serveLogout(w http.ResponseWriter, r *http.Request) {
	session.Clear(r)
	http.Redirect(w, r, "/", 302)
}

func serveErr(w http.ResponseWriter, r *http.Request, err error) {
	msg := err.Error()

	// elastic error handling
	if ee, ok := err.(*elastic.Error); ok {
		if j, err := json.MarshalIndent(ee, "", "  "); err == nil {
			msg = string(j)
		}
	}

	http.Error(w, msg, 500)
	return
}

func setupRoutes() {
	station.Load()

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/routes", serveRoutes)
	http.HandleFunc("/stations", serveStations)
	http.HandleFunc("/station", serveStation)
	http.HandleFunc("/between", serveBetween)
	http.HandleFunc("/users", serveUsers)
	http.HandleFunc("/user", serveUser)
	http.HandleFunc("/bike", serveBike)
	http.HandleFunc("/login", serveLogin)
	http.HandleFunc("/logout", serveLogout)
	http.HandleFunc("/", serveLogin)
}

func serve() {
	setupRoutes()

	var sessionManager session.Middleware
	{
		db, err := bolt.Open("cookies.boltdb", 0600, nil)
		if err != nil {
			panic(err)
		}
		engine := boltstore.New(db, 0)
		sessionManager = session.Manage(engine)
	}

	err := http.ListenAndServe(":8080", sessionManager(http.DefaultServeMux))
	if err != nil {
		panic(err)
	}
}

func serveProd() {
	setupRoutes()
	// engine := memstore.New(0)
	// sessionManager := session.Manage(engine)
	var sessionManager session.Middleware
	{
		db, err := bolt.Open("cookies.boltdb", 0600, nil)
		if err != nil {
			panic(err)
		}
		engine := boltstore.New(db, 0)
		sessionManager = session.Manage(engine)
	}

	var m letsencrypt.Manager
	m.SetHosts([]string{"bikequest.nyc", "www.bikequest.nyc"})
	if err := m.CacheFile("letsencrypt.cache"); err != nil {
		log.Fatal(err)
	}
	log.Fatal(m.ServeHandler(sessionManager(http.DefaultServeMux)))
}

//---------------------------

func betweenAgg(numOrig, numDest int) *elastic.TermsAggregation {
	return elastic.NewTermsAggregation().
		Field("start_station_name").
		Size(numOrig).
		SubAggregation("dest",
			elastic.NewTermsAggregation().Field("end_station_name").Size(numDest),
		)
}

func pluckRides(sr *elastic.SearchResult) []cb.AnonRide {
	var typ cb.AnonRide
	anonRides := []cb.AnonRide{}
	for _, item := range sr.Each(reflect.TypeOf(typ)) {
		if t, ok := item.(cb.AnonRide); ok {
			anonRides = append(anonRides, t)
		}
	}
	return anonRides
}

func pluckQuests(sr *elastic.SearchResult) []quest.Quest {
	var typ quest.Quest
	quests := []quest.Quest{}
	for _, item := range sr.Each(reflect.TypeOf(typ)) {
		if t, ok := item.(quest.Quest); ok {
			quests = append(quests, t)
		}
	}
	return quests
}

func getSess(r *http.Request) *views.Session {
	sess := &views.Session{}
	err := session.GetObject(r, "sess", sess)
	if err != nil {
		session.Clear(r)
	}
	return sess
}
