setup::
	go get -u github.com/valyala/quicktemplate/qtc
	go get -u github.com/cortesi/modd/cmd/modd
	go get -u github.com/cortesi/devd/cmd/devd
	qtc
	go get
	go install

dev::
	modd --bell --notify

sync::
	aws s3 sync s3://tripdata data
	unzip -n 'data/*.zip' -d data

download::
	cat raw_data_urls.txt | xargs -n 1 -P 6 wget -P data/
	unzip -n 'data/*.zip' -d data

flash::
	curl -XDELETE 'localhost:9200/trips?pretty'
	curl -XDELETE 'localhost:9200/stations?pretty'
	curl -XDELETE 'localhost:9200/routes?pretty'
	curl -XDELETE 'localhost:9200/priv?pretty'

	curl -XPUT 'localhost:9200/trips?pretty' -d @mappings/trip.json
	curl -XPUT 'localhost:9200/stations?pretty' -d @mappings/station.json
	curl -XPUT 'localhost:9200/routes?pretty' -d @mappings/route.json
	curl -XPUT 'localhost:9200/priv?pretty' -d @mappings/quest.json

load::
	go install
	curl -XPUT localhost:9200/trips/_settings?pretty -d '{"index" : {"refresh_interval" : "30s"}}'
	curl -XPUT localhost:9200/routes/_settings?pretty -d '{"index" : {"refresh_interval" : "30s"}}'

	time bikequest -load
	curl -XPOST localhost:9200/_refresh

	time bikequest -router
	curl -XPOST localhost:9200/_refresh

	# time bikequest -rank

	curl -XPUT localhost:9200/trips/_settings?pretty -d '{"index" : {"refresh_interval" : "1s"}}'
	curl -XPUT localhost:9200/routes/_settings?pretty -d '{"index" : {"refresh_interval" : "1s"}}'
