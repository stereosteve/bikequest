package es

import (
	"fmt"
	"time"

	"gopkg.in/olivere/elastic.v5"
)

const (
	TripIndex = "trips"
	TripType  = "trip"
)

var (
	Client    *elastic.Client
	Processor *elastic.BulkProcessor
)

func Dial() {
	var err error
	Client, err = elastic.NewClient(
		// in prod systemd starts ES + bikequest at same time
		// so a long connection window is required.
		// TODO: use defaults for dev
		elastic.SetMaxRetries(5),
		elastic.SetHealthcheckTimeoutStartup(time.Second*120),
		// elastic.SetTraceLog(log.New(os.Stdout, "TRACE", log.LstdFlags)),
	)
	if err != nil {
		panic(err)
	}

	newProcessor()
}

func newProcessor() {
	var err error
	Processor, err = Client.BulkProcessor().
		Name("MyBackgroundWorker-1").
		Workers(8).
		Stats(true).
		After(func(executionId int64, requests []elastic.BulkableRequest, response *elastic.BulkResponse, err error) {
			for _, fail := range response.Failed() {
				fmt.Println(fail.Error)
			}
			if err != nil {
				panic(err)
			}
		}).
		Do()
	if err != nil {
		panic(err)
	}
}
