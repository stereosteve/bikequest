Chart.defaults.global.animation.duration = 0;
Chart.defaults.global.hover.animationDuration = 100;

if (USER_AGGS) {

	var labels = []
	var data = []

	USER_AGGS.tripsPerWeek.buckets.forEach(function (b) {
		labels.push(b.key_as_string.split(' ')[0])
		data.push(b.doc_count)
	});

	var ctx = document.getElementById("allTrips");
	new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				label: 'Trips per Week',
				data: data,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});

	// day of week
	var ctx = document.getElementById("dayOfWeek");
	new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
			datasets: [{
				label: 'Day of Week',
				data: DAY_OF_WEEK,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});


	// hour of day
	var ctx = document.getElementById("hourOfDay");
	new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["midnight", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "noon", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"],
			datasets: [{
				label: 'Hour of Day',
				data: HOUR_OF_DAY,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});
}
