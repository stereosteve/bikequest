package main

import (
	"bikequest/es"
	"flag"
)

func main() {

	loadFlag := flag.Bool("load", false, "load data into ElasticSearch")
	routerFlag := flag.Bool("router", false, "extract stations + routes")
	rankFlag := flag.Bool("rank", false, "re-rank rides")
	directionsFlag := flag.Bool("directions", false, "Get bike directions from google maps")
	serveFlag := flag.Bool("serve", false, "start web server")
	serveProdFlag := flag.Bool("serve-prod", false, "start web server with letsencrypt")
	flag.Parse()

	es.Dial()

	if *loadFlag {
		load()
	} else if *rankFlag {
		ranker()
	} else if *directionsFlag {
		directions()
	} else if *serveFlag {
		serve()
	} else if *serveProdFlag {
		serveProd()
	} else if *rankFlag {
		ranker()
	} else if *routerFlag {
		router()
	} else {
		flag.PrintDefaults()
	}
}
