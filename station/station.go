package station

import "bikequest/cb"

type Station struct {
	ID   string      `json:"id"`
	Name string      `json:"name"`
	Loc  cb.Location `json:"loc"`
}
