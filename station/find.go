package station

import (
	"bikequest/es"
	"context"
	"reflect"
	"sync"

	"gopkg.in/olivere/elastic.v5"
)

var (
	mu     sync.RWMutex
	byName map[string]*Station
	byID   map[string]*Station
)

func Pluck(sr *elastic.SearchResult) []*Station {
	var stations []*Station

	var typ *Station
	for _, item := range sr.Each(reflect.TypeOf(typ)) {
		if t, ok := item.(*Station); ok {
			stations = append(stations, t)
		}
	}

	return stations
}

func Load() error {
	mu.Lock()
	defer mu.Unlock()

	sr, err := es.Client.Search("stations").
		Type("station").
		Size(1000).
		Do(context.TODO())
	if err != nil {
		return err
	}

	byName = make(map[string]*Station)
	byID = make(map[string]*Station)

	var typ *Station
	for _, item := range sr.Each(reflect.TypeOf(typ)) {
		if t, ok := item.(*Station); ok {
			byName[t.Name] = t
			byID[t.ID] = t
		}
	}

	return nil
}

func Find(nameOrID string) (*Station, bool) {
	mu.RLock()
	defer mu.RUnlock()

	if s, ok := byID[nameOrID]; ok {
		return s, true
	}
	if s, ok := byName[nameOrID]; ok {
		return s, true
	}
	return nil, false
}
