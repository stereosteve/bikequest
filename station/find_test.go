package station

import (
	"bikequest/es"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFind(t *testing.T) {
	es.Dial()
	err := Load()
	if err != nil {
		t.Fatal(err)
	}

	{
		s, ok := Find("313")
		assert.True(t, ok)
		assert.Equal(t, s.Name, "Washington Ave & Park Ave")
	}
	{
		s, ok := Find("E 55 St & 2 Ave")
		assert.True(t, ok)
		assert.Equal(t, s.ID, "385")
	}
	{
		s, ok := Find("asdfasdf")
		assert.False(t, ok)
		assert.Nil(t, s)
	}
}
