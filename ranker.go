package main

import (
	"bikequest/cb"
	"bikequest/es"
	"context"
	"encoding/json"

	"gopkg.in/olivere/elastic.v5"
)

func ranker() {

	aggDsl := elastic.NewTermsAggregation().
		Field("start_station_name").
		Size(1000).
		SubAggregation("dest",
			elastic.NewTermsAggregation().Field("end_station_name").Size(1000),
		)
	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Aggregation("popularOrigins", aggDsl).
		Size(0).
		Do(context.TODO())

	if err != nil {
		panic(err)
	}

	concurrency := 8
	sem := make(chan bool, concurrency)

	origs, _ := sr.Aggregations.Terms("popularOrigins")
	for _, orig := range origs.Buckets {
		dests, _ := orig.Aggregations.Terms("dest")
		for _, dest := range dests.Buckets {

			sem <- true
			go func(orig, dest string) {
				defer func() { <-sem }()
				rankRoute(orig, dest)
			}(orig.Key.(string), dest.Key.(string))
		}
	}

	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	err = es.Processor.Close()
	if err != nil {
		panic(err)
	}
}

func rankRoute(orig, dest string) {
	// t1 := time.Now()

	dsl := elastic.NewBoolQuery().
		Must(elastic.NewTermQuery("start_station_name", orig)).
		Must(elastic.NewTermQuery("end_station_name", dest))
	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Query(dsl).
		Size(10000).
		Sort("tripduration", true).
		Do(context.TODO())
	if err != nil {
		panic(err)
	}

	for i, hit := range sr.Hits.Hits {
		var ride cb.AnonRide
		err := json.Unmarshal(*hit.Source, &ride)
		if err != nil {
			panic(err)
		}

		ride.RouteRank = i + 1
		update := elastic.NewBulkUpdateRequest().
			Index(es.TripIndex).
			Type(es.TripType).
			Id(hit.Id).
			Doc(ride)
		es.Processor.Add(update)
	}

	// fmt.Printf("%s \t %s \t %d \t %s \n", orig, dest, sr.TotalHits(), time.Since(t1))
}
