package main

import (
	"bikequest/es"
	"bikequest/route"
	"bikequest/station"
	"context"
	"fmt"

	"gopkg.in/olivere/elastic.v5"
)

func router() {
	station.Load()

	aggDsl := elastic.NewTermsAggregation().
		Field("start_station_id").
		Size(1000).
		SubAggregation("dest",
			elastic.NewTermsAggregation().Field("end_station_id").Size(1000),
		)
	sr, err := es.Client.Search(es.TripIndex).
		Type(es.TripType).
		Aggregation("popularOrigins", aggDsl).
		Size(0).
		Do(context.TODO())

	if err != nil {
		panic(err)
	}

	origs, _ := sr.Aggregations.Terms("popularOrigins")
	for _, orig := range origs.Buckets {
		dests, _ := orig.Aggregations.Terms("dest")
		for _, dest := range dests.Buckets {
			origin, _ := station.Find(orig.Key.(string))
			destination, _ := station.Find(dest.Key.(string))
			count := dest.DocCount

			route := route.Route{
				ID:          fmt.Sprintf("%s_%s", origin.ID, destination.ID),
				Origin:      *origin,
				Destination: *destination,
				Count:       count,
			}
			fmt.Println(route)
			indexRoute(route)
		}
	}

	err = es.Processor.Flush()
	if err != nil {
		panic(err)
	}
}

func indexRoute(route route.Route) {
	r := elastic.NewBulkIndexRequest().
		Index("routes").
		Type("route").
		Id(route.ID).
		Doc(route)
	es.Processor.Add(r)
}
