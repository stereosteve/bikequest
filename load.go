package main

import (
	"bikequest/cb"
	"bikequest/es"
	"bikequest/station"
	"crypto/md5"
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gopkg.in/olivere/elastic.v5"

	"github.com/spf13/cast"
)

var seenStation = make(map[string]bool)

func loadFile(path string) {
	fmt.Println("-->", path)
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	csvReader := csv.NewReader(f)

	// skip first row
	_, err = csvReader.Read()
	if err != nil {
		panic(err)
	}

	time1 := time.Now()

	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		anonRide := &cb.AnonRide{
			Duration:         cast.ToInt64(row[0]),
			StartTime:        cb.ParseDate(row[1]),
			StopTime:         cb.ParseDate(row[2]),
			StartStationID:   row[3],
			StartStationName: row[4],
			StartStationLocation: cb.Location{
				Lat: json.Number(row[5]),
				Lon: json.Number(row[6]),
			},
			EndStationID:   row[7],
			EndStationName: row[8],
			EndStationLocation: cb.Location{
				Lat: json.Number(row[9]),
				Lon: json.Number(row[10]),
			},
			BikeID:    row[11],
			UserType:  row[12],
			BirthYear: cast.ToInt(row[13]),
			Gender:    cast.ToInt(row[14]),
			RouteID:   fmt.Sprintf("%s_%s", row[3], row[7]),
		}

		h := md5.New()
		for _, cell := range row {
			io.WriteString(h, cell)
		}
		id := hex.EncodeToString(h.Sum(nil))

		r := elastic.NewBulkIndexRequest().
			Index(es.TripIndex).
			Type(es.TripType).
			Id(id).
			Doc(anonRide)
		es.Processor.Add(r)

		// start station
		if !seenStation[anonRide.StartStationID] {
			seenStation[anonRide.StartStationID] = true
			st := station.Station{
				ID:   anonRide.StartStationID,
				Name: anonRide.StartStationName,
				Loc:  anonRide.StartStationLocation,
			}
			indexStation(st)
		}

		// end station
		if !seenStation[anonRide.EndStationID] {
			seenStation[anonRide.EndStationID] = true
			st := station.Station{
				ID:   anonRide.EndStationID,
				Name: anonRide.EndStationName,
				Loc:  anonRide.EndStationLocation,
			}
			indexStation(st)
		}
	}

	fmt.Println("took", time.Since(time1))
}

func indexStation(st station.Station) {
	r := elastic.NewBulkIndexRequest().
		Index("stations").
		Type("station").
		Id(st.ID).
		Doc(st)
	es.Processor.Add(r)
}

func load() {
	fmt.Println("start:", time.Now())

	files, _ := ioutil.ReadDir("./data")

	for _, f := range files {
		if !strings.HasSuffix(f.Name(), "csv") {
			continue
		}
		loadFile("./data/" + f.Name())
	}

	err := es.Processor.Close()
	if err != nil {
		panic(err)
	}
}
