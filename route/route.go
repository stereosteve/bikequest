package route

import "bikequest/station"

type Route struct {
	ID          string          `json:"id"`
	Origin      station.Station `json:"origin"`
	Destination station.Station `json:"destination"`
	Count       int64           `json:"count"`
}
