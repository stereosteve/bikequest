# BikeQuest

Ride Share leaderboard

## Setup

requires: golang, elasticsearch, nodejs, awscli

```
make
make download
make flash
make index
# wait ~60 mins
bikequest -serve
```

Visit http://localhost:8080

_If you don't want to bother with awscli for download step,
you can use the
[download script](https://github.com/toddwschneider/nyc-citibike-data/blob/master/download_raw_data.sh)
from
[toddwschneider's citibike project](https://github.com/toddwschneider/nyc-citibike-data)_

## Ubuntu Server setup

```
apt-get update && apt-get upgrade -y

wget https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.7.4.linux-amd64.tar.gz
echo 'export GOPATH=~/go' >> .bash_profile
echo 'export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin' >> .bash_profile
source ~/.bash_profile
go version



# install java 8 (http://linux-tips.com/t/how-to-install-java-8-on-debian-jessie/349)

echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | \
  sudo tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | \
  sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
sudo apt-get update
sudo apt-get install oracle-java8-installer


# elasticsearch

wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.0.2.deb
dpkg -i elasticsearch-5.0.2.deb
systemctl enable elasticsearch.service
systemctl start elasticsearch


curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install nodejs -y
node -v

apt-get install git make unzip -y
mkdir -p $GOPATH/src && cd $GOPATH/src
git clone https://bitbucket.org/stereosteve/bikequest.git
cd bikequest
make

```

some notes:

* https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04
* https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04
